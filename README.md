glims-web
==========

Online browser for GLIMS/GTN-G related data sets, including:

* GLIMS Glacier Database (NSIDC)
* Glacier Photograph Collection (NSIDC)
* Fluctuations of Glaciers (WGMS)
* GlaThiDa glacier thickness database (WGMS)
* Randolph Glacier Inventory (NSIDC)
* World Glacier Inventory (NSIDC)
* other related layers for reference

A fully-functioning application requires the presence of [glims-mapserver](https://bitbucket.org/nsidc/glims-mapserver) and [glims-services](https://bitbucket.org/nsidc/glims-services) VMs built under the same (e.g. `integration`, `qa`, `staging`, etc.) environment as glims-web itself.

## Deployment

`vagrant nsidc up --env=<env>` will provision the system and expose the following URLs:

* `<env>.glims.org/maps/glims.html` -> `/srv/www/glims.html` (GLIMS Application)
* `<env>.glims.org/maps/gtng.html`  -> `/srv/www/gtng.html` (GTN-G Application)
* `<env>.glims.org/maps/info.html?anlys_id=<glacier_id>`  -> `/srv/www/info.html` (Glacier Info Application)
* `<env>.glims.org/maps/rcinfo.html?rc_id=<rc_id>` -> `srv/www/rcinfo.html` (Regional Center info page)

The application is also available via NSIDC-internal URLs, e.g. `<env>.glims-web.apps.int.nsidc.org/glims.html`.

## Logging

Only static content is served (by nginx, as deployed) directly by the glims-web application. Logs are kept in `/var/log/nginx` and rotated nightly to `/share/logs/glims`.

## Development Notes

* You must use the [`handlebars` program to precompile](http://handlebarsjs.com/precompilation.html) `source/templates/layer_query_tables` into `source/templates/layer_query_tables.js`. Your version of `handlebars` must match that of `handlebars.runtime.min.js` declared in `source/glims.html` and `source/gtng.html`.

* The names of NSIDC development (`--env=dev`) VMs contain a username component, unknown to the VMs themselves. Therefore, it is currently not possible to provision [glims-mapserver](https://bitbucket.org/nsidc/glims-mapserver), [glims-services](https://bitbucket.org/nsidc/glims-services) and [glims-web](https://bitbucket.org/nsidc/glims-web) VMs such that they have knowledge of each other's hostnames to communicate with one another.
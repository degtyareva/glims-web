var rc_id = $.url('?rc_id');
if (!rc_id) {
  var notifyOpts = {className: 'error', autoHide: false};
  $.notify('Parameter ?rc_id=<int> Undefined', notifyOpts);
}

function generateTables(data, names) {

  sections = ['rc_info'];
  var affl_people;
  var dns = names.displaynames;

  // check for key affiliated (if its empty)
  if (data.rc_info.affiliated !== undefined) {
    affl_people = data.rc_info.affiliated;
    delete data.rc_info.affiliated;
  }

  $.each(sections, function(_, tab) {
    if ($.isEmptyObject(data[tab])) {
      $('#rcinfo').append('<div class="panel panel-warning">' +
                          '<div class="panel-heading">No Records Available</div></div>');
    } else {
      var center = data[tab];
      for (var i = 0; i < dns.length; i++) {
        var keyname = dns[i][0];
	var displayname = dns[i][1];
	var item = center[keyname];
        if (item) $('#rcinfo').append('<tr><td class="col-md-5">' + displayname + '</td><td>' + item + '</td></tr>');
      }
    }
  });

  if (affl_people !== undefined) {
    $.each(affl_people, function(_, affl) {
      var affl_row = $('<tr>');
      affl_row.append($('<td>').text(affl.contact_id));
      affl_row.append($('<td>').text(affl.surname + ', ' + affl.givennames));
      affl_row.append($('<td>').text(affl.affiliation));
      affl_row.append($('<td>').text(affl.city));
      affl_row.append($('<td>').text(affl.country_code));
      affl_row.append($('<td>').text(affl.url));
      affl_row.append($('<td>').text(affl.email_primary));
      $('#rcaffl').append(affl_row);
    });
  } else {
    $('#rcaffl').replaceWith('<div class="panel panel-warning">' +
                             '<div class="panel-heading">No Records Available</div></div>');
  }

}

var spinner = new Spinner(spinnerOpts).spin(document.getElementById('rcinfo'));

var glacierinfo = function(names){
  $.ajax({
    dataType: 'json',
    url: serviceURL + '/rcinfo/' + rc_id,
    success: function(data, textStatus, jqXHR) {
      generateTables(data, names);
      spinner.stop();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      $.notify('/rcinfo Service Failed: ' + textStatus);
      spinner.stop();
    }
  });
};

var placenames = function(){
  $.ajax({
    dataType: 'json',
    url: serviceURL + '/displaynames',
    success: function(data, textStatus, jqXHR) {
      glacierinfo(data);
      spinner.stop();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      $.notify('/displaynames Service Failed: ' + textStatus);
      spinner.stop();
    }
  });
};

placenames();

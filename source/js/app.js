
// Map Selection Handler

map.on('singleclick', function(e) {
  $('#mapSelectionDetails .modal-body').empty();

  // get a list of all possible visible layer names
  var lyrNames = [];
  var viewResolution = map.getView().getResolution();
  ol.control.LayerSwitcher.forEachRecursive(map, function(lyr) {
    var lyrType = lyr.get('type') || lyr.get('mytype');
    if ($.inArray(lyrType, ['base', 'group']) === -1){
      if (lyr.getVisible()){
        lyrNames.push(lyr.getSource().getParams().LAYERS);
      }
    }
  });

  var lyrNamesStr = lyrNames.toString();

  var getFeatureInfoParams = {
    'INFO_FORMAT': 'application/json',
    'QUERY_LAYERS': lyrNamesStr,
    'LAYERS': lyrNamesStr,
    'FEATURE_COUNT': 20};

  var url = fogSource.getGetFeatureInfoUrl(e.coordinate, viewResolution,
                                           'EPSG:3857', getFeatureInfoParams);

  // call with second arg of 0 at this level
  get_feature_info_robustly(url, 0);

});


// Recursive function to detect and handle redirects
function get_feature_info_robustly(url, level) {
    $.ajax({
      type: 'get',
      dataType: 'text',
      url: url,
      success: loadQueryTables,
      error: function (jqxhr, textStatus, error, level) {
        if (jqxhr.status == 302 && level == 0) {
            // call with second arg of 1 to break infinite loop
            get_feature_info_robustly(jqxhr.getResponseHeader("Location"), 1);
        }
        else {
            $.notify('Data Retrieval Failed', 'error');
        }
      }
    }
    );
}


// Callback for map query

var loadQueryTables = function (data) {

  data = JSON.parse(data);

  if (! Object.keys(data).find(function(key) {return ! $.isEmptyObject(data[key]); })) {
    $.notify('No Results Found', 'warn');
    return;
  }

  // Add glac_ids as string to data object for download button to use
  if (!$.isEmptyObject(data['glac_lines'])) {
    glac_ids = []
    $.each(data['glac_lines'], function(i, glac_obj) {
      if (!$.isEmptyObject(glac_obj)) {
        glac_ids.push(glac_obj['glac_id'])
      }
    });
    data.glacIdStr = glac_ids.join();
  }

  if (!$.isEmptyObject(data['glims_points'])) {
    glac_ids = []
    $.each(data['glims_points'], function(i, glac_obj) {
      if (!$.isEmptyObject(glac_obj)) {
        glac_ids.push(glac_obj['glac_id'])
      }
    });
    data.glacIdStr = glac_ids.join();
  }

  // combine RGI layer data into single list
  // (dirty hack since mapserver template not using layer group)
  var rgiLayersAll = [
      'Alaska',
      'AntarcticSubantarctic',
      'ArcticCanadaNorth',
      'ArcticCanadaSouth',
      'CaucasusMiddleEast',
      'CentralAsia',
      'CentralEurope',
      'GreenlandPeriphery',
      'Iceland',
      'LowLatitudes',
      'NewZealand',
      'NorthAsia',
      'RussianArctic',
      'Scandinavia',
      'SouthAsiaEast',
      'SouthAsiaWest',
      'SouthernAndes',
      'Svalbard',
      'WesternCanadaUS'
  ];
  var rgiLayers = rgiLayersAll.filter(function(el) {
    return Object.keys(data).indexOf(el) != -1;
  });
  if (rgiLayers.length > 0) {
    rgi40Data = [];
    for (var i = 0; i< rgiLayers.length; i++) {
      if (data[rgiLayers[i]] !== undefined) {
        rgi40Data = rgi40Data.concat(data[rgiLayers[i]]);
      }
    }
    data.rgi40 = rgi40Data;
  }

  data.gtng = gtng;

  // Modify float precision
  if (!$.isEmptyObject(data['WGI_points'])) {
    $.each(data['WGI_points'], function(i, wgi_obj) {
      if (!$.isEmptyObject(wgi_obj)) {
        if (!$.isEmptyObject(wgi_obj['total_area'])) {
          wgi_obj['total_area'] = parseFloat(wgi_obj['total_area']).toFixed(2);
        }
        if (!$.isEmptyObject(wgi_obj['mean_length'])) {
          wgi_obj['mean_length'] = parseFloat(wgi_obj['mean_length']).toFixed(2);
        }
      }
    })
      }

  if (!$.isEmptyObject(data['rgi40'])) {
    $.each(data['rgi40'], function(i, rgi_obj) {
      if (!$.isEmptyObject(rgi_obj)) {
        if (!$.isEmptyObject(rgi_obj['AREA'])) {
          rgi_obj['AREA'] = parseFloat(rgi_obj['AREA']).toFixed(2);
        }
      }
    })
      }

  if (!$.isEmptyObject(data['glac_lines'])) {
    $.each(data['glac_lines'], function(i, glac_obj) {
      if (!$.isEmptyObject(glac_obj)) {
        if (!$.isEmptyObject(glac_obj['db_area'])) {
          glac_obj['db_area'] = parseFloat(glac_obj['db_area']).toFixed(2);
        }
      }
    })
      }

  if (!$.isEmptyObject(data['glims_points'])) {
    $.each(data['glims_points'], function(i, glac_obj) {
      if (!$.isEmptyObject(glac_obj)) {
        if (!$.isEmptyObject(glac_obj['db_area'])) {
          glac_obj['db_area'] = parseFloat(glac_obj['db_area']).toFixed(2);
        }
      }
    })
      }

  $('#mapSelectionDetails .modal-body').html(Handlebars.templates.layer_query_tables(data));
  $("#mapSelectionDetails").modal('show');
};

// Search Submission Handler

var renderPlacename = function(data){
  if (data.totalResultsCount === 0) {
    var notifyOpts = {position: 'bottom', className: 'warn', autoHideDelay: 2000};
    $('#placename-input').notify('No Results Found', notifyOpts);
  } else {
    var lng = parseFloat(data.geonames[0].lng);
    var lat = parseFloat(data.geonames[0].lat);
    map.setView(
      new ol.View({
        center: ol.proj.transform([lng, lat], 'EPSG:4326', 'EPSG:3857'),
        zoom: 12
      })
    );
    $('#placename-input').val('');
  }
};

// Search Form Handler

$('#placename-search').on('submit', function(e) {
  e.preventDefault();
  var spinner = new Spinner(spinnerOpts).spin(document.getElementById('wrapper'));
  $.ajax({
    url: 'http://api.geonames.org/search',
    dataType: 'json',
    data: {type: 'json', username: 'kpurdon', name: $('#placename-input').val()},
    success: function(data, textStatus, jqXHR) {
      renderPlacename(data);
      spinner.stop();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      $.notify('Search Failed');
      spinner.stop();
    }
  });
});

$('#download-current').click(function(e) {
  // cleanup when replaced with python service
  var currentExtent = map.getView().calculateExtent(map.getSize());
  var latLonExtentMin = ol.proj.transform(currentExtent.slice(0,2), 'EPSG:3857', 'EPSG:4326');
  var latLonExtentMax = ol.proj.transform(currentExtent.slice(2), 'EPSG:3857', 'EPSG:4326');
  var params = {
    minx: latLonExtentMin[0],
    miny: latLonExtentMin[1],
    maxx: latLonExtentMax[0],
    maxy: latLonExtentMax[1],
    proj_string: 'proj=latlong,ellps=WGS84,no_defs'
  };
  window.open(getDataUrl + $.param(params), '_blank');
});

$('#download-all').click(function(e) {
  window.open(getAllDataUrl, '_blank');
});

var anlys_id = $.url('?anlys_id');
if (!anlys_id) {
  var notifyOpts = {className: 'error', autoHide: false};
  $.notify('Parameter ?anlys_id=<int> Undefined', notifyOpts);
}

/*
 Map Rendering
 */
var renderMap = function(data) {

  // Define Background Layers
  var mqSat = new ol.layer.Tile({
    title: 'MapQuest Satellite',
    type: 'base',
    visible: false,
    source: new ol.source.MapQuest({layer: 'sat'})
  });
  var mqOsm = new ol.layer.Tile({
    title: 'MapQuest Open Street Map',
    type: 'base',
    visible: true,
    source: new ol.source.MapQuest({layer: 'osm'})
  });
  var backgroundLayers = new ol.layer.Group({
    'title': 'Base maps',
    mytype: 'group',
    layers: [mqSat, mqOsm]
  });

  // Define Data Layers
  var glimsGlaciersSource  = new ol.source.ImageWMS ({
    url: mapservURL,
    params: { LAYERS:"GLIMS_GLACIERS" },
    servertype: 'mapserver'
  });
  var glimsGlaciersLayer = new ol.layer.Image({
    title: 'GLIMS glacier outlines<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img align="middle" src="' +
      mapservURL + getlegend + '&layer=glac_lines"/>',
    source: glimsGlaciersSource
  });
  var gtngOverlays = new ol.layer.Group({
    title: 'Glacier Databases',
    mytype: 'group',
    layers: [glimsGlaciersLayer]
  });

  var wktFmt = new ol.format.WKT();
  var centerFeature = wktFmt.readFeature(data.astext);
  var centerArr = centerFeature.getGeometry().flatCoordinates;

  var map = new ol.Map({
    target: 'infomap',
    layers: [backgroundLayers, gtngOverlays],
    view: new ol.View({
      center: ol.proj.transform(centerArr, 'EPSG:4326', 'EPSG:3857'),
      zoom: 10
    })
  });

  map.addControl(new ol.control.LayerSwitcher());
  map.addControl(new ol.control.ScaleLine());


  /*
   When the "Glacier Map" tab is opened the map needs to
   be updated to the size of the viewport since the tab
   pane is hidded when the map is initially rendered.
   */
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    if (e.target.text === 'Glacier Map') {
      map.updateSize();
    }
  });
};

function generateHypsometry(data){

  if (!$.isEmptyObject(data.hyps_info)) {

    c3.generate({
      bindto: '#hypsometry',
      size: {
        height: 500
      },
      data: {
        json: data.hyps_info.data,
        keys: {
          x: 'bin',
          value: ['area', 'cum_area']
        },
        axes: {
          cum_area: 'y2'
        }
      },
      legend: {position: 'inset', inset: {anchor: 'top-right', y: 60, x: 20}},
      axis: {
        x: {min: 0, label: {text: 'Elevation (m)', position: 'outer-center'}},
        y: {min: 0, label: {text: 'Area (km^2)', position: 'outer-middle'}},
        y2: {min: 0, show: true, label: {text: 'Cumulative Area (km^2)', position: 'outer-middle'}}
      },
      color: {
        pattern: ['#0000CC', '#FF0000']
      }
    });
  } else {
    $('#hypsometry').append('<div class="panel panel-warning">' +
                            '<div class="panel-heading">No records available</div></div>');
  };
}

function sort_records(records, keys) {
  var compare_unequal_items = function(item1, item2) {
    if (   item1 && ! item2 ) return -1;
    if ( ! item1 &&   item2 ) return +1;
    if (   item1 &&   item2 ) return item1 < item2 ? -1 : +1;
  };
  var compare = function(item1, item2) {
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      if (item1[key] !== item2[key]) {
        return compare_unequal_items(item1[key], item2[key]);
      };
    };
    return 0;
  };
  records.sort(compare);
}

function generateTables(data, names) {

  var currentTime = Date.now();
  var dns = names.displaynames;
  var lit_refs;
  var sections = ['glac_info', 'anlys_info', 'subm_info'];
  var tr = function(key, val) { return '<tr><td class="col-md-5">' + key + '</td><td>' + val + '</td></tr>'; };

  // replace subm_info rcid with link
  if (data.subm_info.rc_id !== undefined) {
    var rcid = data.subm_info.rc_id;
    data.subm_info.rc_id = '<a href="rcinfo.html?rc_id=' + rcid + '">' + rcid + '</a>';
  }

  // glac_info (possible lit_refs)
  if (data.glac_info.lit_refs !== undefined) {
    lit_refs = data.glac_info.lit_refs;
    delete data.glac_info.lit_refs;
  }

  // general tables
  $.each(sections, function(_, tab) {
      if ($.isEmptyObject(data[tab])) {
      $('#' + tab).append('<div class="panel panel-warning">' +
                          '<div class="panel-heading">No records available</div></div>');
    } else {
      if (tab === 'anlys_info') {
        var analysts = data[tab];
        sort_records(analysts, ['surname', 'givennames', 'analyst_id']);
        $.each(analysts, function(_, analyst) {
          var analyst_id = 'analyst_' + analyst.analyst_id;
          $('.analyst_info').append('<table id="' + analyst_id + '" class="table table-condensed table-hover"></table>');
          $('#' + analyst_id).append('<caption>Analyst Record</caption>');
          for (var i = 0; i < dns.length; i++) {
            var keyname = dns[i][0], displayname = dns[i][1], item = analyst[keyname];
            if (item) $('#' + analyst_id).append(tr(displayname, item));
          }
        });
      } else {
        for (var i = 0; i < dns.length; i++) {
          var keyname = dns[i][0], displayname = dns[i][1], item = data[tab][keyname];
          if (item) $('.' + tab).append(tr(displayname, item));
        }
      }
    }
  });

  // render lit_refs if they exist
  if (lit_refs !== undefined) {
    $('#glac_info').append("<h2>Literature References</h2><ul>");
    $.each(lit_refs, function(_, ref) {
      // author_names (year) title. journal volume (num):pages.
      var ref_fmt = ref.author_names + " (" + ref.year + "). ";
      ref_fmt = ref_fmt + (ref.title == undefined ? "" : ref.title + ".");
      ref_fmt = ref_fmt + (ref.journal == undefined ? "" : " <i>" + ref.journal + "</i>");
      ref_fmt = ref_fmt + (ref.volume == undefined ? "" : " <b>" + ref.volume + "</b>");
      ref_fmt = ref_fmt + (ref.num == undefined ? "" : " (" + ref.num + ")");
      ref_fmt = ref_fmt + (ref.pages == undefined ? "" : ":" + ref.pages) + ".";
      $('#glac_info').append("<li>" + ref_fmt + "</li>");
    });
    $('#glac_info').append("</ul>");
  } else {
    $('#glac_info').append('<div class="panel panel-warning">' +
                           '<div class="panel-heading">No literature references available</div></div>');
  }

  // img_info (sub-tables of image records)
  if ($.isEmptyObject(data.img_info)) {
    $('#img_info').append('<div class="panel panel-warning">' +
                          '<div class="panel-heading">No records available</div></div>');
  } else {
    var img_info = data.img_info;
    sort_records(img_info, ['acq_timestamp', 'orig_id', 'image_id']);
    $.each(img_info, function(_, img) {
      var sub_table = "<table class='table table-condensed table-hover'>";
      sub_table = sub_table.concat("<caption>Image Record</caption>");
      for (var i = 0; i < dns.length; i++) {
        var keyname = dns[i][0], displayname = dns[i][1], item = img[keyname];
        if (item) sub_table = sub_table.concat(tr(displayname, item));
      }
      sub_table = sub_table.concat('</table>');
      $('.img_info').append(sub_table);
    });
  }

  // map_info (sub-tables of map records)
  if ($.isEmptyObject(data.map_info)) {
    $('#map_info').append('<div class="panel panel-warning">' +
                          '<div class="panel-heading">No records available</div></div>');
  } else {
    $.each(data.map_info, function(_, map) {
      var sub_table = "<table class='table table-condensed table-hover'>";
      sub_table = sub_table.concat("<caption>Map Record</caption>");
      for (var i = 0; i < dns.length; i++) {
        var keyname = dns[i][0], displayname = dns[i][1], item = map[keyname];
        if (item) sub_table = sub_table.concat(tr(displayname, item));
      }
      sub_table = sub_table.concat('</table>');
      $('.map_info').append(sub_table);
    });
  }

  // download (needs to check for embargo)
  if (currentTime >= Date.parse(data.embargo.release_okay_date)) {
    url = "http://glims.colorado.edu/php_utils/get_data.php?glac_id=" + data.glac_info.glacier_id
    downloadLink = '<a href="' + url + '">' + 'Download' + '</a>';
    $('#download').append('<p>The GLIMS Glacier Outlines for this glacier may be downloaded in a number of formats: ' + downloadLink + '</p>');
  } else {
    embargoText = '<p>Sorry, this glacier\'s data is currently under embargo, meaning that access to ' +
      'the data has been restricted at the request of the analyst. After the embargo ' +
      'period has ended the data will become available for download. </p>' +
      '<p>The embargo period ends on: ' + data.embargo.release_okay_date + '</p>';
    $('#download').append(embargoText);
  }

} // generateTables

var generateMap = function(){
    $.ajax({
      dataType: 'json',
      url: serviceURL + '/glacierbounds',
      data: {anlys_id: anlys_id},
      success: function(data, textStatus, jqXHR) {
        renderMap(data);
        spinner.stop();
      },
      error: function(jqXHR, textStatus, errorThrown) {
        $.notify('/glacierbounds Service Failed: ' + textStatus);
        spinner.stop();
      }
    });
};

var spinner = new Spinner(spinnerOpts).spin(document.getElementById('glac_info'));

var glacierinfo = function(names){
  $.ajax({
    dataType: 'json',
    url: serviceURL + '/glacierinfo',
    data: {anlys_id: anlys_id},
    success: function(data, textStatus, jqXHR) {
      generateTables(data, names);
      generateHypsometry(data);
      generateMap();
      spinner.stop();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      $.notify('/glacierinfo Service Failed: ' + textStatus);
      spinner.stop();
    }
  });
};

var placenames = function(){
  $.ajax({
    dataType: 'json',
    url: serviceURL + '/displaynames',
    success: function(data, textStatus, jqXHR) {
      glacierinfo(data);
      spinner.stop();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      $.notify('/displaynames Service Failed: ' + textStatus);
      spinner.stop();
    }
  });
}();

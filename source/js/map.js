/*
 Map Decleration
 */

// Define background layers

var mqSat = new ol.layer.Tile({
  title: 'MapQuest Satellite',
  type: 'base',
  visible: false,
  source: new ol.source.MapQuest({layer: 'sat'})
});

var mqOsm = new ol.layer.Tile({
  title: 'MapQuest Open Street Map',
  type: 'base',
  visible: true,
  source: new ol.source.MapQuest({layer: 'osm'})
});

var backgroundLayers = new ol.layer.Group({
  'title': 'Base maps',
  mytype: 'group',
  layers: [mqSat, mqOsm]
});

// DCW data layer

var dcwSource = new ol.source.ImageWMS({
  url: mapservURL,
  params: { LAYERS:"dcw_glaciers" },
  servertype: 'mapserver'
});

var dcwLayer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=dcw_glaciers"/>',
  source: dcwSource,
  visible: false
});

// FOG data layer

var fogSource = new ol.source.ImageWMS({
  url: mapservURL,
  params: { LAYERS:"FOG_points" },
  servertype: 'mapserver'
});

var fogLayer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=FOG_points"/>',
  source: fogSource
});

// GlaThiDa data layer

var glathidaSource = new ol.source.ImageWMS({
  url: mapservURL,
  params: { LAYERS:"glathida_points" },
  servertype: 'mapserver'
});

var glathidaLayer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=glathida_points"/>',
  source: glathidaSource
});

// GLIMS data layer

var glimsGlaciersSource  = new ol.source.ImageWMS({
  url: mapservURL,
  params: { LAYERS:"GLIMS_GLACIERS" },
  servertype: 'mapserver'
});

var glimsGlaciersLayer = new ol.layer.Image({
  title: 'GLIMS glacier outlines<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img align="middle" src="' +
    mapservURL + getlegend + '&layer=glac_lines"/>',
  source: glimsGlaciersSource
});

// GLIMS RC Outlines data layer

var glimsRegionsSource = new ol.source.ImageWMS({
  url: mapservURL,
  params: { LAYERS:"GLIMS_Regions" },
  servertype: 'mapserver'
});

var glimsRegionsLayer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=GLIMS_Regions"/>',
  source: glimsRegionsSource,
  visible: false
});

// GPC data layer

var gpcSource = new ol.source.ImageWMS({
  url: mapservURL,
  params: { LAYERS:"glacier_photos" },
  servertype: 'mapserver'
});

var gpcLayer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=glacier_photos"/>',
  source: gpcSource
});

// RGI40 data layer

var rgiSource = new ol.source.ImageWMS({
  url: mapservURL,
  params: { LAYERS:"rgi40" },
  servertype: 'mapserver'
});

var rgiLayer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=WesternCanadaUS"/>',
  source: rgiSource
});

// RGI O1 Regions data layer

var rgiO1Source = new ol.source.ImageWMS({
  url: mapservURL,
  params: { LAYERS:"rgi3_o1regions" },
  servertype: 'mapserver'
});

var rgiO1Layer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=rgi3_o1regions"/>',
  source: rgiO1Source,
  visible: false
});

// RGI O2 Regions data layer

var rgiO2Source = new ol.source.ImageWMS({
  url: mapservURL,
  params: { LAYERS:"rgi3_o2regions" },
  servertype: 'mapserver'
});

var rgiO2Layer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=rgi3_o2regions"/>',
  source: rgiO2Source,
  visible: false
});

// WGI data layer

var wgiSource = new ol.source.ImageWMS({
  url: mapservURL,
  params: { LAYERS:"wgi_points" },
  servertype: 'mapserver'
});

var wgiLayer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=WGI_points"/>',
  source: wgiSource
});

// Define layer groupings

var glims_layers = [gpcLayer, wgiLayer, dcwLayer, glimsRegionsLayer, rgiO2Layer, rgiO1Layer, rgiLayer, glimsGlaciersLayer];
var gtng_layers = [gpcLayer, fogLayer, glathidaLayer, wgiLayer, rgiLayer, glimsGlaciersLayer];

var gtngOverlays = new ol.layer.Group({
  title: 'Glacier Databases',
  mytype: 'group',
  layers: gtng ? gtng_layers : glims_layers
});

// Define the Map

var mapview = new ol.View({
  center: ol.proj.transform([-70, -3], 'EPSG:4326', 'EPSG:3857'),
  zoom: 5,
  minZoom: 1,
  maxZoom: 14
});

var map = new ol.Map({
  target: 'map',
  layers: [backgroundLayers,  gtngOverlays],
  view: mapview
});

// Add map controls

var mousePositionControl = new ol.control.MousePosition({
  coordinateFormat: ol.coordinate.createStringXY(4),
  projection: 'EPSG:4326',
  undefinedHTML: '&nbsp;'
});
map.addControl(mousePositionControl);
map.addControl(new ol.control.LayerSwitcher());
map.addControl(new ol.control.ScaleLine());

var lowleft = ol.proj.transform([-180, -60], 'EPSG:4326', 'EPSG:3857');
var upright = ol.proj.transform([180, 60], 'EPSG:4326', 'EPSG:3857');
var extent_control = new ol.control.ZoomToExtent({
  extent: [ lowleft[0], lowleft[1], upright[0], upright[1] ]
});
map.addControl(extent_control);

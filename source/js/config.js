// Environment Configuration (This variable is modified by Puppet!)

var environment = 'integration';

// Base URL Configuration

var URLPrefix = 'http://' + (environment === 'production' ? 'www' : environment) + '.glims.org';
var mapservURL = URLPrefix + '/mapservice';
var serviceURL = URLPrefix + '/services';

// Map Request Options

var getlegend = '?version=1.1.1&format=PNG&request=GetLegendGraphic&height=201&service=WMS';

// Spinner Options

var spinnerOpts = {opacity: 0.25, lines: 15, length: 10, width: 1, scale: 3};

// TODO Clean up when replaced with python service

var getDataUrl = 'http://glims.colorado.edu/php_utils/get_data_byextent.php?';
var getAllDataUrl = 'http://www.glims.org/download';

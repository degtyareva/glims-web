(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['layer_query_tables'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {};

  return "  <form name=send_extent action=\"http://glims.colorado.edu/php_utils/get_data.php\" target=\"_blank\" method=POST>\n    <table class='table table-condensed table-hover'>\n      <caption>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.gtng : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "        <br/>\n        <input type=submit value='Download these GLIMS glacier outlines'>\n      </caption>\n      <tr>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Static' target=\"_blank\">Glacier Name</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Static' target=\"_blank\">Glacier ID</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Dynamic' target=\"_blank\">Analysis ID</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Dynamic' target=\"_blank\">Area, km<sup>2</sup></a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Image' target=\"_blank\">Acquisition Date</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Package_Info' target=\"_blank\">Date Available</a>\n        </th>\n        <th>More Info</th>\n      </tr>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.glims_points : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "      <input type='hidden' name='glac_ids' value='"
    + container.escapeExpression(((helper = (helper = helpers.glacIdStr || (depth0 != null ? depth0.glacIdStr : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"glacIdStr","hash":{},"data":data}) : helper)))
    + "'>\n    </table>\n  </form>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.gtn-g.org/data_catalogue_glims.html\" target=\"_blank\" class=\"glims_info\">GLIMS Glacier Outlines</a>\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.glims.org/\" target=\"_blank\" class=\"glims_info\">GLIMS Glacier Outlines</a>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <tr>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.glac_name || (depth0 != null ? depth0.glac_name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"glac_name","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.glac_id || (depth0 != null ? depth0.glac_id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"glac_id","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.anlys_id || (depth0 != null ? depth0.anlys_id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"anlys_id","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.db_area || (depth0 != null ? depth0.db_area : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"db_area","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.src_date || (depth0 != null ? depth0.src_date : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"src_date","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.release_date || (depth0 != null ? depth0.release_date : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"release_date","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">\n            <a href=\"info.html?anlys_id="
    + alias4(((helper = (helper = helpers.anlys_id || (depth0 != null ? depth0.anlys_id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"anlys_id","hash":{},"data":data}) : helper)))
    + "\" target=\"_blank\">More...</a>\n          </div></td>\n        </tr>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {};

  return "  <form name=send_extent action=\"http://glims.colorado.edu/php_utils/get_data.php\" target=\"_blank\" method=POST>\n    <table class='table table-condensed table-hover'>\n      <caption>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.gtng : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "        <br/>\n        <input type=submit value='Download these GLIMS Glacier Outlines'>\n      </caption>\n      <tr>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Static' target=\"_blank\">Glacier Name</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Static' target=\"_blank\">Glacier ID</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Dynamic' target=\"_blank\">Analysis ID</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Dynamic' target=\"_blank\">Area, km<sup>2</sup></a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Image' target=\"_blank\">Acquisition Date</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Submission_info' target=\"_blank\">Date Available</a>\n        </th>\n        <th>More Info</th>\n      </tr>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.glac_lines : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "      <input type='hidden' name='glac_ids' value='"
    + container.escapeExpression(((helper = (helper = helpers.glacIdStr || (depth0 != null ? depth0.glacIdStr : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"glacIdStr","hash":{},"data":data}) : helper)))
    + "'>\n    </table>\n  </form>\n";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {};

  return "  <table class='table table-condensed table-hover'>\n    <caption>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.gtng : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.program(13, data, 0),"data":data})) != null ? stack1 : "")
    + "    </caption>\n    <tr>\n      <th>Glacier Name</th>\n      <th>RGI Glacier ID</th>\n      <th>Begin date</th>\n      <th>End date</th>\n      <th>Total Area, km<sup>2</sup></th>\n      <th>Minimum Elevation, m</th>\n      <th>Median Elevation, m</th>\n      <th>Maximum Elevation, m</th>\n      <th>Glacier type</th>\n    </tr>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.rgi40 : depth0),{"name":"each","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  </table>\n";
},"11":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.gtn-g.org/data_catalogue_rgi.html\" target=\"_blank\" class='rgi_info'>Randolph Glacier Inventory version 4.0</a>\n";
},"13":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.glims.org/RGI/\" target=\"_blank\" class='rgi_info'>Randolph Glacier Inventory version 4.0</a>\n";
},"15":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "      <tr>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.NAME || (depth0 != null ? depth0.NAME : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"NAME","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.RGIID || (depth0 != null ? depth0.RGIID : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RGIID","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.BGNDATE || (depth0 != null ? depth0.BGNDATE : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"BGNDATE","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.ENDDATE || (depth0 != null ? depth0.ENDDATE : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ENDDATE","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.AREA || (depth0 != null ? depth0.AREA : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AREA","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.ZMIN || (depth0 != null ? depth0.ZMIN : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ZMIN","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.ZMED || (depth0 != null ? depth0.ZMED : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ZMED","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.ZMAX || (depth0 != null ? depth0.ZMAX : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ZMAX","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.GLACTYPE || (depth0 != null ? depth0.GLACTYPE : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"GLACTYPE","hash":{},"data":data}) : helper)))
    + "</div></td>\n      </tr>\n";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "  <table class='table table-condensed table-hover'>\n    <caption>\n      <a href=\"http://glims.colorado.edu/php_utils/nsidc_rc_table_public.php\" target=\"_blank\" class='glims_regions'>GLIMS Regions</a>\n    </caption>\n    <tr>\n      <th>RC ID</th>\n      <th>Affiliation</th>\n      <th>Geographic Location</th>\n      <th>Given Names</th>\n      <th>Surname</th>\n      <th>Primary URL</th>\n    </tr>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.RC_Outlines : depth0),{"name":"each","hash":{},"fn":container.program(18, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  </table>\n";
},"18":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "      <tr>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.rc_id || (depth0 != null ? depth0.rc_id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"rc_id","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.affiliation || (depth0 != null ? depth0.affiliation : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"affiliation","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.geog_area || (depth0 != null ? depth0.geog_area : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"geog_area","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.givennames || (depth0 != null ? depth0.givennames : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"givennames","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.surname || (depth0 != null ? depth0.surname : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"surname","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\"><a href=\""
    + alias4(((helper = (helper = helpers.rc_url_prime || (depth0 != null ? depth0.rc_url_prime : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"rc_url_prime","hash":{},"data":data}) : helper)))
    + "\" target=\"_blank\">"
    + alias4(((helper = (helper = helpers.rc_url_prime || (depth0 != null ? depth0.rc_url_prime : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"rc_url_prime","hash":{},"data":data}) : helper)))
    + "</a></div></td>\n      </tr>\n";
},"20":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {};

  return "  <table class='table table-condensed table-hover'>\n    <caption>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.gtng : depth0),{"name":"if","hash":{},"fn":container.program(21, data, 0),"inverse":container.program(23, data, 0),"data":data})) != null ? stack1 : "")
    + "    </caption>\n    <tr>\n      <th>Political Unit</th>\n      <th>Glacier Name</th>\n      <th>WGI Glacier ID</th>\n      <th>Total Area, km<sup>2</sup></th>\n      <th>Mean Length</th>\n      <th>Min. Elevation, m</th>\n      <th>Mean Elevation, m</th>\n      <th>Max. Elevation, m</th>\n      <th>Photo Year</th>\n      <th>Topo Year</th>\n      <th>Data Contributor</th>\n    </tr>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.WGI_points : depth0),{"name":"each","hash":{},"fn":container.program(25, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  </table>\n";
},"21":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.gtn-g.org/data_catalogue_wgi.html\" class='wgi_info' target='_blank'>World Glacier Inventory</a>\n";
},"23":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://nsidc.org/data/glacier_inventory/\" target='_blank' class='wgi_info'>World Glacier Inventory</a>\n";
},"25":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "      <tr>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.political_unit || (depth0 != null ? depth0.political_unit : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"political_unit","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.glacier_name || (depth0 != null ? depth0.glacier_name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"glacier_name","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.wgi_glacier_id || (depth0 != null ? depth0.wgi_glacier_id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"wgi_glacier_id","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.total_area || (depth0 != null ? depth0.total_area : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_area","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.mean_length || (depth0 != null ? depth0.mean_length : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"mean_length","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.min_elev || (depth0 != null ? depth0.min_elev : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"min_elev","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.mean_elev || (depth0 != null ? depth0.mean_elev : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"mean_elev","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.max_elev || (depth0 != null ? depth0.max_elev : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"max_elev","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.photo_year || (depth0 != null ? depth0.photo_year : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"photo_year","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.topo_year || (depth0 != null ? depth0.topo_year : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"topo_year","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.data_contributor || (depth0 != null ? depth0.data_contributor : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"data_contributor","hash":{},"data":data}) : helper)))
    + "</div></td>\n      </tr>\n";
},"27":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "  <table class='table table-condensed table-hover'>\n      <caption>\n        <a href=\"http://www.gtn-g.org/data_catalogue_glathida.html\"\n           target=\"_blank\" class=\"glathida_info\">Glacier Thickness Dataset</a>\n      </caption>\n    <tr>\n      <th>Political Unit</th>\n      <th>Glacier Name</th>\n      <th>Record ID</th>\n      <th>Mean Thickness</th>\n      <th>Max Thickness</th>\n      <th>Method</th>\n      <th>Survey Date</th>\n      <th>Principal Investigator</th>\n      <th>Agency</th>\n      <th>References</th>\n    </tr>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.glathida_points : depth0),{"name":"each","hash":{},"fn":container.program(28, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  </table>\n";
},"28":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "      <tr>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.PU || (depth0 != null ? depth0.PU : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"PU","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.NAME || (depth0 != null ? depth0.NAME : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"NAME","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.GlaThiDaID || (depth0 != null ? depth0.GlaThiDaID : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"GlaThiDaID","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.MeanDepth || (depth0 != null ? depth0.MeanDepth : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"MeanDepth","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.MaxDepth || (depth0 != null ? depth0.MaxDepth : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"MaxDepth","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.Method || (depth0 != null ? depth0.Method : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Method","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.SurveyDate || (depth0 != null ? depth0.SurveyDate : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"SurveyDate","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.PI || (depth0 != null ? depth0.PI : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"PI","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.AGENCY || (depth0 != null ? depth0.AGENCY : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AGENCY","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.REFERENCES || (depth0 != null ? depth0.REFERENCES : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"REFERENCES","hash":{},"data":data}) : helper)))
    + "</div></td>\n      </tr>\n";
},"30":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "  <table class='table table-condensed table-hover'>\n    <caption>\n      <a href=\"http://www.gtn-g.org/data_catalogue_fog.html\" target=\"_blank\" class=\"fog_info\">Fluctuations of Glaciers</a>\n    </caption>\n    <tr>\n      <th>Political Unit</th>\n      <th>Glacier Name</th>\n      <th>WGMS ID</th>\n      <th>Measurement type</th>\n      <th>Num Observation</th>\n      <th>1st Ref. Year</th>\n      <th>1st Survey Year</th>\n      <th>Last Survey Year</th>\n      <th>Current Status</th>\n      <th>Principal Investigator</th>\n      <th>See graph</th>\n      <th>Get minimal data series</th>\n      <th>Order full data series</th>\n    </tr>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.FOG_points : depth0),{"name":"each","hash":{},"fn":container.program(31, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  </table>\n";
},"31":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <tr>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.PU || (depth0 != null ? depth0.PU : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"PU","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.NAME || (depth0 != null ? depth0.NAME : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"NAME","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.WGMS_ID || (depth0 != null ? depth0.WGMS_ID : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"WGMS_ID","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.source_des || (depth0 != null ? depth0.source_des : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"source_des","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.NoObs || (depth0 != null ? depth0.NoObs : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"NoObs","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.firstRY || (depth0 != null ? depth0.firstRY : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"firstRY","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.firstSY || (depth0 != null ? depth0.firstSY : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"firstSY","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.lastSY || (depth0 != null ? depth0.lastSY : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"lastSY","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.CurrentSta || (depth0 != null ? depth0.CurrentSta : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"CurrentSta","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.LatestPI || (depth0 != null ? depth0.LatestPI : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"LatestPI","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\"><a href='"
    + alias4(((helper = (helper = helpers.GraphLink || (depth0 != null ? depth0.GraphLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"GraphLink","hash":{},"data":data}) : helper)))
    + "' target='_blank'>See graph</a></div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\"><a href='"
    + alias4(((helper = (helper = helpers.DataLink || (depth0 != null ? depth0.DataLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"DataLink","hash":{},"data":data}) : helper)))
    + "'>Get data</a></div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\"><a href='"
    + alias4(((helper = (helper = helpers.MailToLink || (depth0 != null ? depth0.MailToLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"MailToLink","hash":{},"data":data}) : helper)))
    + "'>Order full data</a></div></td>\n        </tr>\n";
},"33":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {};

  return "  <table class='table table-condensed table-hover'>\n    <caption>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.gtng : depth0),{"name":"if","hash":{},"fn":container.program(34, data, 0),"inverse":container.program(36, data, 0),"data":data})) != null ? stack1 : "")
    + "    </caption>\n    <tr>\n      <th>Country</th>\n      <th>Glacier Name</th>\n      <th>Photo ID</th>\n      <th>Photo Date</th>\n      <th>Photographer Name</th>\n      <th>Source</th>\n      <th>More Info</th>\n    </tr>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.glacier_photos : depth0),{"name":"each","hash":{},"fn":container.program(38, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  </table>\n";
},"34":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.gtn-g.org/data_catalogue_gpc.html\" target=\"_blank\" class=\"gpc_info\">Glacier Photograph Collection</a>\n";
},"36":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://nsidc.org/data/glacier_photo/\" target=\"_blank\" class=\"gpc_info\">Glacier Photograph Collection</a>\n";
},"38":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "      <tr>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.country || (depth0 != null ? depth0.country : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"country","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.glacier_name || (depth0 != null ? depth0.glacier_name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"glacier_name","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.photo_id || (depth0 != null ? depth0.photo_id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"photo_id","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.photo_year || (depth0 != null ? depth0.photo_year : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"photo_year","hash":{},"data":data}) : helper)))
    + "-"
    + alias4(((helper = (helper = helpers.photo_month || (depth0 != null ? depth0.photo_month : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"photo_month","hash":{},"data":data}) : helper)))
    + "-"
    + alias4(((helper = (helper = helpers.photo_day || (depth0 != null ? depth0.photo_day : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"photo_day","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.photographer_name || (depth0 != null ? depth0.photographer_name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"photographer_name","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.source || (depth0 != null ? depth0.source : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"source","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">\n          <a href=\"http://nsidc.org/data/glacier_photo/search/gpd_deliver_jpg.pl?"
    + alias4(((helper = (helper = helpers.photo_id || (depth0 != null ? depth0.photo_id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"photo_id","hash":{},"data":data}) : helper)))
    + "\" target=\"_blank\">More...</a>\n        </div></td>\n      </tr>\n";
},"40":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "  <table class='table table-condensed table-hover'>\n    <tr>\n      <td><div style=\"max-height:80px; overflow:auto;\">\n        <h2>\n          Countries\n        </h2>\n      </div></td>\n    </tr>\n    <tr>\n      <th>Country Name</th>\n      <th>Country Code</th>\n      <th>Population</th>\n      <th>Area, km<sup>2</sup></th>\n    </tr>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.Countries : depth0),{"name":"each","hash":{},"fn":container.program(41, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  </table>\n";
},"41":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <tr>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.cntry_name || (depth0 != null ? depth0.cntry_name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"cntry_name","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.country_code2 || (depth0 != null ? depth0.country_code2 : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"country_code2","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.pop_cntry || (depth0 != null ? depth0.pop_cntry : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"pop_cntry","hash":{},"data":data}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = helpers.sqkm_cntry || (depth0 != null ? depth0.sqkm_cntry : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"sqkm_cntry","hash":{},"data":data}) : helper)))
    + "</div></td>\n        </tr>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {};

  return ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.glims_points : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.glac_lines : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.rgi40 : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.RC_Outlines : depth0),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.WGI_points : depth0),{"name":"if","hash":{},"fn":container.program(20, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.glathida_points : depth0),{"name":"if","hash":{},"fn":container.program(27, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.FOG_points : depth0),{"name":"if","hash":{},"fn":container.program(30, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.glacier_photos : depth0),{"name":"if","hash":{},"fn":container.program(33, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.Countries : depth0),{"name":"if","hash":{},"fn":container.program(40, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
})();